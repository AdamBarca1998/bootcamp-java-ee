<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="User">
	<jsp:attribute name="body">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>email</th>
				<th>password</th>
				<th>nickname</th>
				<th>firstName</th>
				<th>surname</th>
				<th>birthday</th>
				<th>age</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><c:out value="${user.id}" /></td>
				<td><c:out value="${user.email}" /></td>
				<td><c:out value="${user.password}" /></td>
				<td><c:out value="${user.nickname}" /></td>
				<td><c:out value="${user.firstName}" /></td>
				<td><c:out value="${user.surname}" /></td>
				<td><c:out value="${user.birthday}" /></td>
				<td><c:out value="${user.age}" /></td>
				<td><a href="${pageContext.request.contextPath}/user/address?id=${user.id}" class="btn btn-primary">address</a></td>
				<td><a href="${pageContext.request.contextPath}/user/contact?id=${user.id}" class="btn btn-primary">contact</a></td>
			</tr>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>