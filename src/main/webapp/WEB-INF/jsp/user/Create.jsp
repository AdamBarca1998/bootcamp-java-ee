<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Persons">
	<jsp:attribute name="body">
	<!-- Top content -->
	<div class="top-content">
		<div class="inner-bg">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text">
						<p></p>
						<h1>
							<strong>Bootcamp CGI Training</strong>
						</h1>
						<div class="description">
							<p></p>
						</div>
					</div>
				</div>
				
				
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3 form-box">
						<div class="form-bottom">"src/main/java/com/cgi/api/UserBasicViewDto.java"
							<c:if test="${param.error != null}">
								<div class="alert alert-danger">
									<p>Invalid username and password.</p>
								</div>
							</c:if>
							<c:if test="${param.logout != null}">
								<div class="alert alert-success">
									<p>You have been logged out successfully.</p>
								</div>
							</c:if>
							
							<c:url value="/user/create" var="loginUrl" />
							<form role="form" action="${loginUrl}" method="post"
									class="login-form">
								
								<div class="form-group">
									<label class="sr-only" for="email">Email</label> <input
											type="text" name="email" placeholder="Email..."
											class="form-username form-control" id="form-username">
								</div>
								
								<div class="form-group">
									<label class="sr-only" for="password">Password</label> <input
											type="password" name="password" placeholder="Password..."
											class="form-password form-control" id="form-password">
								</div>
								
								<div class="form-group">
									<label class="sr-only" for="nickname">Nickname</label> <input
											type="text" name="nickname" placeholder="Nickname..."
											class="form-username form-control" id="form-username">
								</div>
								
								<div class="form-group">
									<label class="sr-only" for="firstName">FirstName</label> <input
											type="text" name="firstname" placeholder="FirstName..."
											class="form-username form-control" id="form-username">
								</div>
								
								<div class="form-group">
									<label class="sr-only" for="surname">Surname</label> <input
											type="text" name="surname" placeholder="Surname..."
											class="form-username form-control" id="form-username">
								</div>
								
								<div class="form-group">
									<label class="sr-only" for="age">Age</label> <input
											type="text" name="age" placeholder="Age..."
											class="form-username form-control" id="form-username">
								</div>
								
								<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								<button type="submit" class="btn">Sign in!</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</jsp:attribute>
</my:pagetemplate>