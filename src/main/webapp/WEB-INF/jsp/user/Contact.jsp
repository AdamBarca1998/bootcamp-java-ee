<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="User">
	<jsp:attribute name="body">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>contact</th>
				<th>title</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${contacts}" var="contact">
				<tr>
					<td><c:out value="${contact.getContact()}" /></td>
					<td><c:out value="${contact.getTitle()}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>