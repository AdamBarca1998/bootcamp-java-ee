<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Users">
	<jsp:attribute name="body">
	<h1 class="page-header">
		List of all persons in the system <small></small>
	</h1>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>city</th>
				<th>street</th>
				<th>houseNumber</th>
				<th>zipCode</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${addresses}" var="address">
				<tr>
					<td><c:out value="${address.getCity()}" /></td>
					<td><c:out value="${address.getStreet()}" /></td>
					<td><c:out value="${address.getHouseNumber()}" /></td>
					<td><c:out value="${address.getZipCode()}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>