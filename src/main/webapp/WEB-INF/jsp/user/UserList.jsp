<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Users">
	<jsp:attribute name="body">
	<h1 class="page-header">
		List of all persons in the system <small></small>
	</h1>
	
	<div>
		<a href="${pageContext.request.contextPath}/user/create" class="btn btn-primary">create user</a>
	</div>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>email</th>
				<!--  <th>password</th>-->
				<th>nickname</th>
				<th>firstName</th>
				<th>surname</th>
				<!--<th>birthday</th>-->
				<th>age</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${users}" var="user">
				<tr>
					<td><c:out value="${user.id}" /></td>
					<td><c:out value="${user.email}" /></td>
					<!--<td><c:out value="${user.password}" /></td>-->
					<td><c:out value="${user.nickname}" /></td>
					<td><c:out value="${user.firstName}" /></td>
					<td><c:out value="${user.surname}" /></td>
					<!--<td><c:out value="${user.birthday}" /></td>-->
					<td><c:out value="${user.age}" /></td>
					
					<td><a href="${pageContext.request.contextPath}/user/detail?id=${user.id}" class="btn btn-primary">view</a></td>
					<td><a href="${pageContext.request.contextPath}/user/delete?id=${user.id}" class="btn btn-primary">delete</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>