<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Bootcamp CGI error!</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link
	href="${pageContext.request.contextPath}/assets/css/errorpage-style.css"
	rel="stylesheet" type="text/css" media="all" />

<link href='http://fonts.googleapis.com/css?family=Fenix'
	rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="wrap">
		<div class="main">
			<h3>Bootcamp CGI</h3>
			<h1>Oops Could not found</h1>
			<p>
				<div id="throwable">
					<c items="${error}" var="error">
						<c:out value="${error.getThrowable()}"/>
					</c>
				</div>
				
				<div id="statusCode">
					<c:out value="${error.getStatusCode()}"/>
				</div>
				
				<div id="servletName">
					<c:out value="${error.getServletName()}"/>
				</div>
				
				
				There's a lot of reasons why this page is<span class="error">
					404</span>.<br> <span>Don't waste your time enjoying the look
					of it</span>
			</p>
			<div class="search">
				<form>
					<input type="text" onfocus="this.value = '';"
						onblur="if (this.value == '') {this.value = 'Enter ur email';}"
						value="Enter ur email"> <input type="submit"
						value="Submit">
				</form>
			</div>
			<div class="icons">
				<p>Follow us on:</p>
				<ul>
					<li><a href="#"><img
							src="${pageContext.request.contextPath}/assets/images/img1.png"></a></li>
					<li><a href="#"><img
							src="${pageContext.request.contextPath}/assets/images/img2.png"></a></li>
					<li><a href="#"><img
							src="${pageContext.request.contextPath}/assets/images/img3.png"></a></li>
					<li><a href="#"><img
							src="${pageContext.request.contextPath}/assets/images/img4.png"></a></li>
					<li><a href="#"><img
							src="${pageContext.request.contextPath}/assets/images/img5.png"></a></li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>