<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Users">
	<jsp:attribute name="body">
	<h1 class="page-header">
		List of all persons in the system <small></small>
	</h1>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>first name</th>
				<th>surname</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${people}" var="human">
				<tr>
					<td><c:out value="${human.getFirstName()}" /></td>
					<td><c:out value="${human.getSurname()}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>