<th>id</th>
				<th>email</th>
				<th>password</th>
				<th>nickname</th>
				<th>firstName</th>
				<th>surname</th>
				<th>birthday</th>
				<th>age</th>user<%@ tag pageEncoding="utf-8" dynamic-attributes="dynattrs"%>
<%@ attribute name="title" required="false"%>
<%@ attribute name="subtitle" required="false"%>
<%@ attribute name="head" fragment="true"%>
<%@ attribute name="body" fragment="true" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="${pageContext.request.locale}">
<!--  head starts -->
<head>
<!-- CSS -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet"
	href="<c:url value="/assets/bootstrap/css/bootstrap.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/assets/font-awesome/css/font-awesome.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/assets/css/form-elements.css"/>">
<link rel="stylesheet" href="<c:url value="/assets/css/style.css"/>">

<!-- Favicon and touch icons -->
<link rel="shortcut icon"
	href="<c:url value="/assets/ico/favicon.png"/>">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<c:url value="/assets/ico/apple-touch-icon-144-precomposed.png"/>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<c:url value="/assets/ico/apple-touch-icon-114-precomposed.png"/>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<c:url value="/assets/ico/apple-touch-icon-72-precomposed.png"/>">
<link rel="apple-touch-icon-precomposed"
	href="<c:url value="/assets/ico/apple-touch-icon-57-precomposed.png"/>">
<!--//end-animate-->
<jsp:invoke fragment="head" />
</head>

<body>
	<!--header-->
	<div class="header">
		<nav class="navbar navbar-inverse">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="${pageContext.request.contextPath}/">Bootcamp
						CGI DEMO</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a
						href="${pageContext.request.contextPath}/">Bootcamp CGI DEMO</a></li>
					<!--<li><a href="${pageContext.request.contextPath}/create">Create</a></li>-->
					<!--<li><a href="${pageContext.request.contextPath}/contact">Contacts</a></li>-->
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li><a href="${pageContext.request.contextPath}/logout"><span
							class="glyphicon glyphicon-log-in"> Logout</span></a></li>
				</ul>
			</div>
		</nav>
	</div>
	<!--//header-->

	<div class="container">
		<!-- page body -->
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<!-- page body -->
				<jsp:invoke fragment="body" />
			</div>
		</div>
	</div>
	
	
	<!-- js -->
	<script src="<c:url value="/assets/bootstrap/js/bootstrap.js"/>"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<c:url value="/assets/js/jquery-1.11.1.min.js"/>"></script>
	<!-- <script src="<c:url value="/assets/bootstrap/js/bootstrap.min.js"/>"></script> -->
	<script src="<c:url value="/assets/js/jquery.backstretch.min.js"/>"></script>
	<script src="<c:url value="/assets/js/scripts.js"/>"></script>
</body>
</html>