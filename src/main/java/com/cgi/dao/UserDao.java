package com.cgi.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.api.AddressViewDto;
import com.cgi.api.ContactViewDto;
import com.cgi.api.CreateUserDto;
import com.cgi.api.UserBasicViewDto;
import com.cgi.servlets.ErrorHandlerServlet;

@Stateless
public class UserDao {
	
	@Resource(lookup="jdbc/bootcampCGI")
	private DataSource dataSource;
	
	public List<UserBasicViewDto> findAll() {
		List<UserBasicViewDto> users = new ArrayList<>();
		
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT id_user, email, password, "
						+ "nickname, first_name, surname, "
						+ "birthday, age FROM user u")) {
			while (rs.next()) {
				UserBasicViewDto user = new UserBasicViewDto();
				user.setId(rs.getLong("id_user"));
				user.setEmail(rs.getString("email"));
				//user.setPassword(rs.getString("password"));
				user.setNickname(rs.getString("nickname"));
				user.setFirstName(rs.getString("first_name"));
				user.setSurname(rs.getString("surname"));
				//user.setBirthday(rs.getString("birthday"));
				user.setAge(rs.getInt("age"));
				
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public UserBasicViewDto findById(Long id) {
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT id_user, email, password, nickname, first_name, surname, birthday, age, id_address FROM user u WHERE id_user = " + id)) {
			while (rs.next()) {
				UserBasicViewDto user = new UserBasicViewDto();
				user.setId(rs.getLong("id_user"));
				user.setEmail(rs.getString("email"));
				user.setPassword(rs.getString("password"));
				user.setNickname(rs.getString("nickname"));
				user.setFirstName(rs.getString("first_name"));
				user.setSurname(rs.getString("surname"));
				user.setBirthday(rs.getString("birthday"));
				user.setAge(rs.getInt("age"));
				user.setId_address(rs.getLong("id_address"));
				
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void delete(Long id) {
		try (Connection connection = dataSource.getConnection()) {;
		Statement statement = connection.createStatement();
		statement.executeUpdate("DELETE FROM user WHERE user.id_user = " + id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//INSERT INTO user 
	//(email, password, nickname, first_name, surname, birthday, age, id_address) 
	//VALUES("dasd", "asdsad", "asdasd", "sadsa", "sadsa", "sadasd" , 11, 2)
	public void createUser(CreateUserDto newUser) {
		/*SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");  
		Date date = new Date();  
		String stringDate = formatter.format(date); */
		
		
		try (Connection connection = dataSource.getConnection()) {;
				Statement statement = connection.createStatement();
				statement.executeUpdate("INSERT INTO user (email, password, nickname, first_name, surname, birthday, age, id_address) VALUES('katerina.oasdadqwva@seznam.cz', '$2a$06$cjXTOlpy.GZULB/atthy3.i/QkJjwBkUxXYAsd', 'Dvarfka', 'Kateřina', 'Hrušková', 1980-05-16, 32, 40)");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	//+ "VALUES(\"" + newUser.getEmail() + "\", \"" + newUser.getPassword() + "\", \"" + newUser.getNickname() + 
	//"\", \"" + newUser.getFirstName() + "\", \"" + newUser.getSurname() + "\", " + stringDate + ", " + newUser.getAge() + ", " + 2 + ")");
	
	public UserBasicViewDto findLast() {
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT MAX(id_user) FROM user")) {
			while (rs.next()) {
				Long id = rs.getLong("MAX(id_user)");
				
				return findById(id);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public List<ContactViewDto> findAllContacts(Long id) {
		List<ContactViewDto> contacts = new ArrayList<>();
		
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT contact, title FROM contact_type ct, contact c, user u WHERE ct.id_contact_type = c.id_contact_type and c.id_user = u.id_user and u.id_user = " + id)) {
			while (rs.next()) {
				ContactViewDto contact = new ContactViewDto();
				contact.setContact(rs.getString("contact"));
				contact.setTitle(rs.getString("title"));
				
				contacts.add(contact);
			}
			return contacts;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public List<AddressViewDto> findAllAddress(Long id) {
		List<AddressViewDto> addresses = new ArrayList<>();
		
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT city, street, house_number, zip_code FROM address a, user u "
						+ "WHERE a.id_address = u.id_address and u.id_user = " + id)) {
			while (rs.next()) {
				AddressViewDto address = new AddressViewDto();
				address.setCity(rs.getString("city"));
				address.setStreet(rs.getString("street"));
				address.setHouseNumber(rs.getString("house_number"));
				address.setZipCode(rs.getString("zip_code"));
				
				addresses.add(address);
			}
			
			return addresses;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return null;
	}
}
