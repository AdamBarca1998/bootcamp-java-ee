package com.cgi.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.api.RoleViewDto;
import com.cgi.api.UserBasicViewDto;

@Stateless
public class RoleDao {
	
	@Resource(lookup="jdbc/bootcampCGI")
	private DataSource dataSource;
	
	
	//SELECT u.first_name, u.surname FROM role r, role_has_user rhu, user u WHERE r.id_role = rhu.id_role and rhu.id_user = u.id_user and r.title = 'USER'
	public List<RoleViewDto> findRole(String title) {
		List<RoleViewDto> people = new ArrayList<>();
		
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT u.first_name, u.surname "
						+ "FROM role r, role_has_user rhu, user u "
						+ "WHERE r.id_role = rhu.id_role and rhu.id_user = u.id_user and r.title = '" + title + "'")) {
			while (rs.next()) {
				RoleViewDto human = new RoleViewDto();
				human.setFirstName(rs.getString("first_name"));
				human.setSurname(rs.getString("surname"));
			
				people.add(human);
			}
			return people;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
