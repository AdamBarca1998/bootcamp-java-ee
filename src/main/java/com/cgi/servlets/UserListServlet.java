package com.cgi.servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.AddressViewDto;
import com.cgi.api.ContactViewDto;
import com.cgi.api.CreateUserDto;
import com.cgi.api.UserBasicViewDto;
import com.cgi.exceptions.ResourceNotFoundException;
import com.cgi.service.UserService;

/**
 * Servlet implementation class PersonListServlet
 */
@WebServlet(name = "UserListServlet", urlPatterns = { "/user/*" })
public class UserListServlet extends HttpServlet {       
	@EJB
	private UserService userService;
	
	 /**
     * @see HttpServlet#HttpServlet()
     */
	public UserListServlet() {
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		if (pathInfo != null && pathInfo.contains("/detail")) {
			String id = request.getParameter("id");
						
			try {
				UserBasicViewDto user = userService.findById(Long.parseLong(id));
				request.setAttribute("user", user);
				request.getRequestDispatcher("/WEB-INF/jsp/user/User.jsp").forward(request, response);
			} catch (Exception ex) {
				throw new ResourceNotFoundException(ex.getMessage());
			}
		} else if (pathInfo != null && pathInfo.contains("/delete")) {
			String id = request.getParameter("id");
			
			try {
				userService.delete(Long.parseLong(id));
				
				List<UserBasicViewDto> users = userService.findAll();
				request.setAttribute("users", users);
				request.getRequestDispatcher("/WEB-INF/jsp/user/UserList.jsp").forward(request, response);
			} catch (Exception ex) {
				throw new ResourceNotFoundException(ex.getMessage());
			}
		} else if (pathInfo != null && pathInfo.contains("/list")) {
			List<UserBasicViewDto> users = userService.findAll();
			
			request.setAttribute("users", users);
			request.getRequestDispatcher("/WEB-INF/jsp/user/UserList.jsp").forward(request, response);
		} else if (pathInfo != null && pathInfo.contains("/create")) {	
			request.getRequestDispatcher("/WEB-INF/jsp/user/Create.jsp").forward(request, response);
		} else if (pathInfo != null && pathInfo.contains("/last")) {
			try {
				//UserBasicViewDto user = userService.findLast();
				
				//request.setAttribute("user", user);
				request.getRequestDispatcher("/WEB-INF/jsp/user/User.jsp").forward(request, response);
			} catch (Exception ex) {
				throw new ResourceNotFoundException(ex.getMessage());
			}
		} else if (pathInfo != null && pathInfo.contains("/contact")) {
			String id = request.getParameter("id");
			
			try {
				List<ContactViewDto> contacts = userService.findAllContacts(Long.parseLong(id));
		
				request.setAttribute("contacts", contacts);
				request.getRequestDispatcher("/WEB-INF/jsp/user/Contact.jsp").forward(request, response);
			} catch (Exception ex) {
				throw new ResourceNotFoundException(ex.getMessage());
			}
		} else if (pathInfo != null && pathInfo.contains("/address")) {
			String id = request.getParameter("id");
			
			try {
				List<AddressViewDto> addresses = userService.findAllAddress(Long.parseLong(id));
		
				request.setAttribute("addresses", addresses);
				request.getRequestDispatcher("/WEB-INF/jsp/user/Address.jsp").forward(request, response);
			} catch (Exception ex) {
				throw new ResourceNotFoundException(ex.getMessage());
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String nickname = request.getParameter("nickname");
		String firstName = request.getParameter("firstName");
		String surname = request.getParameter("surname");
		int age = 18; 
				
		try {		
			int ageTmp = Integer.parseInt(request.getParameter("age"));
			age = ageTmp;
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		CreateUserDto user = new CreateUserDto();
		user.setEmail(email);
		user.setPassword(password);
		user.setNickname(nickname);
		user.setFirstName(firstName);
		user.setSurname(surname);
		user.setAge(age);
	
		userService.createUser(user);
		
		UserBasicViewDto lastUser = userService.findLast();
		request.setAttribute("user", lastUser);
		request.getRequestDispatcher("/WEB-INF/jsp/user/User.jsp").forward(request, response);
	}
}
