package com.cgi.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.ErrorViewDto;

@WebServlet(name = "ErrorHandlerServlet", urlPatterns = { "/error-handler" })
public class ErrorHandlerServlet extends HttpServlet {

    public ErrorHandlerServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Analyze the servlet exception
		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		String servletName = (String) request.getAttribute("javax.servlet.error.servlet_name");
		
		ErrorViewDto error = new ErrorViewDto();
		error.setThrowable(throwable);
		error.setStatusCode(statusCode);
		error.setServletName(servletName);
		
		request.setAttribute("error", error);
		request.getRequestDispatcher("/WEB-INF/jsp/error/Error.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
