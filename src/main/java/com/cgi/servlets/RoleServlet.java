package com.cgi.servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.RoleViewDto;
import com.cgi.api.UserBasicViewDto;
import com.cgi.exceptions.ResourceNotFoundException;
import com.cgi.service.RoleService;
import com.cgi.service.UserService;


/**
 * Servlet implementation class RoleServlet
 */
@WebServlet(name = "RoleServlet", urlPatterns = { "/role" })
public class RoleServlet extends HttpServlet {
	
	@EJB
	private RoleService roleService;
		
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String role = request.getParameter("role");
						
		try {
			List<RoleViewDto> people = roleService.findPeople(role);
			request.setAttribute("people", people);
			
			request.getRequestDispatcher("/WEB-INF/jsp/role/Role.jsp").forward(request, response);
		} catch (Exception ex) {
				throw new ResourceNotFoundException(ex.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
