package com.cgi.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.cgi.api.RoleViewDto;
import com.cgi.dao.RoleDao;
import com.cgi.dao.UserDao;

@Stateless
public class RoleService {

	@EJB
	private RoleDao roleDao;
	
	public List<RoleViewDto> findPeople(String title) {
		return roleDao.findRole(title);
	}
}
