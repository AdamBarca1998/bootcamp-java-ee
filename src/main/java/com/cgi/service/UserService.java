package com.cgi.service;

import java.util.*;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.transaction.Transactional;

import com.cgi.api.AddressViewDto;
import com.cgi.api.ContactViewDto;
import com.cgi.api.CreateUserDto;
import com.cgi.api.UserBasicViewDto;
import com.cgi.dao.UserDao;

@Stateless
public class UserService {
	
	@EJB
	private UserDao userDao;
	
	//@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public List<UserBasicViewDto> findAll() {
		return userDao.findAll();
	}
	
	//@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public UserBasicViewDto findById(long id) {
		return userDao.findById(id);
	}
	
	
	public void createUser(CreateUserDto newUser) {
		userDao.createUser(newUser);
	}
	
	public UserBasicViewDto findLast() {
		return userDao.findLast();
	}
	
	public void delete(long id) {
		userDao.delete(id);
	}
	
	public List<ContactViewDto> findAllContacts(long id) {
		return userDao.findAllContacts(id);
	}
	
	public List<AddressViewDto> findAllAddress(long id) {
		return userDao.findAllAddress(id);
	}
}
