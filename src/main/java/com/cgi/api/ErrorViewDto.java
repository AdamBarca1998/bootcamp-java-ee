package com.cgi.api;

public class ErrorViewDto {
	private Throwable throwable;
	private Integer statusCode;
	private String servletName;
	
	public Throwable getThrowable() {
		return throwable;
	}
	
	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getServletName() {
		return servletName;
	}
	
	public void setServletName(String servletName) {
		this.servletName = servletName;
	}
}
